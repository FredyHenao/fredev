<?php

use PHPUnit\Framework\TestCase;
use Fredev\AccessHandler as Access;
use Fredev\AuthenticatorInterface;
use Fredev\User;

class AccessHandlerTest extends TestCase
{
    public function tearDown(): void
    {
        Mockery::close();
    }

    public function test_grant_access()
    {
        $access = new Access($this->getAuthenticatorMock());

        $this->assertTrue(
            $access->check('admin')
        );
    }

    public function test_deny_access()
    {

        
        $access = new Access($this->getAuthenticatorMock());

        $this->assertFalse(
            $access->check('editor')
        );
    }

    public function getAuthenticatorMock()
    {
        $user = Mockery::mock(User::class);
        $user->role = 'admin';

        $auth = Mockery::mock(AuthenticatorInterface::class);
        $auth->shouldReceive('check')
            ->once()
            ->andReturn(true);
        $auth->shouldReceive('user')
            ->once()
            ->andReturn('user');

        return $auth;
    }
}