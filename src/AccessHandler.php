<?php

namespace Fredev;

class AccessHandler 
{

    /**
     *
     * @var \Fredev\AuthenticatorInterface
     */
    protected $auth;

    /**
     *
     * @param \Fredev\AuthenticatorInterface $auth
     */
    public function __construct(AuthenticatorInterface $auth)
    {
        $this->auth = $auth;
    }

    /**
     *
     * @param [String] $role
     * @return void
     */
    public function check($role)
    {
        return $this->auth->check() && $this->auth->user()->role === $role;
    }
}