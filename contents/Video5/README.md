## Introducción a Mockery

Bienvenidos a la lección 5 del curso creación de componentes para PHP y Laravel. En las clases anteriores, te explicábamos los conceptos básicos necesarios para la creación de un nuevo componente en PHP a través de Composer, incluyendo temas como stubs, inyección de dependencias y por supuesto, el uso de pruebas unitarias para evaluar el funcionamiento de una parte de nuestro código de manera aislada.

En la clase de hoy aprenderemos un nuevo componente:  Mockery.

Mockery es un componente de PHP que nos ayuda a escribir pruebas usando objetos simulados para imitar el comportamiento de objetos reales de una forma controlada.

### Conocimientos previos
Te recomendamos estar al tanto con las lecciones anteriores de este curso, antes de avanzar, para que comprendas mejor los temas que van a ser tratados en este video:

* Creación de un nuevo proyecto con Composer y PHPUnit
* Dependencias y código acoplado
* Creación y uso de Interfaces y Stubs
* Pruebas de integración en Laravel 5.1

### Notas
Para instalar mockery podemos usar composer ejecutando la siguiente linea en la consola de comandos

```php
    composer require --dev mockery/mockery
```

Con esto ya tendremos disponible el paquete en nuestro entorno local.

### Actividad
1. Clona el repositorio para descargar el código.
2. Ejecuta git checkout 1.5 para moverte al tag de esta lección.
3. Crea un nuevo branch a partir de ese tag, git checkout -b activity1
4. Ve al archivo de pruebas  tests/AccessHandlerTest.php y encontrarás 3 métodos de prueba nuevos con sus respectivos comentarios.
5. Modifica el código en src/AccessHandler.php ¿Puedes hacer que las pruebas pasen?
6. Si lo logras envíanos el código a través de un Pull Request, sino, puedes discutirlo y solicitar ayuda en el canal #curso4  de nuestro Slack.