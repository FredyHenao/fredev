<?php

namespace Fredev;

interface AuthenticatorInterface 
{
    public function check();

    public function user();
}