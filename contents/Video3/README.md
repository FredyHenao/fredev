## Inyección de dependencias

Esta es la lección 3 del curso Crea componentes para PHP y Laravel.

En esta oportunidad se tratará un punto importante de la programación orientada a objetos, no sólo en PHP sino en general, como es la inyección de dependencia que servirá de mucha ayuda para desacoplar las clases creadas en la lección anterior: AccessHandler y sobretodo SessionManager.

### Conocimientos previos
Posiblemente en la lección anterior te quedaron dudas sobre porque se crearon métodos y propiedades con la palabra clave static. Esto se debe a que al crear una propiedad o método de esa manera no se necesita instanciar la clase para poder usarlos.  Es decir, las propiedades y método declarados como «static» están asociados a una clase en lugar de a una instancia de la clase (objeto).

Sin embargo, esto trae como consecuencia el alto acoplamiento de las clases, como lo vimos en la lección anterior.  Para dar una solución a esto se puede usar inyección de dependencias, la cual es un patrón de diseño orientado a objetos que nos permite programar de una manera que se pueda reutilizar código y que se pruebe fácilmente.

Si quieres repasar puedes darle un vistazo a nuestro Curso de programación orientada a objetos con PHP  y así afianzar los conocimientos de cómo se crean las clases, propiedades, métodos, se instancia una clase, sobre el método __construct, entre otros temas.

## Notas
En el video, se define $this como una pseudovariable que hace referencia al contexto del objeto con el cual se está trabajando y que se diferencia de las propiedades o métodos definidos con la palabra clave static que tienen un alcance global.