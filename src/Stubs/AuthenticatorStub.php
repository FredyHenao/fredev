<?php

namespace Fredev\Stubs;

use Fredev\AuthenticatorInterface;
use Fredev\User;

class AuthenticatorStub implements AuthenticatorInterface
{
    private $logged;

    public function __construct($logged = true)
    {
        $this->logged = $logged;
    }

    public function check()
    {
        return $this->logged;
    }

    public function user()
    {
        return new User([
            'role' => 'admin'
        ]);
    }
}